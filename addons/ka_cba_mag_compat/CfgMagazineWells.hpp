class CfgMagazineWells
{
	class CBA_12g_2rnds
	{
		KA_Mags[] =
		{
			//2 round sawed off shotgun shells
			"KICKASS_2Rnd_Sawed_Off_Shotgun_Pellets"
		};
	};
	class CBA_12g_6rnds
	{
		KA_Mags[] =
		{
			//6 round 12 gauge shells
			"6Rnd_M1014_buck",
			"6Rnd_M1014_PPA",
			"6Rnd_M1014_slug"
		};
	};
	class CBA_12g_8rnds
	{
		KA_Mags[] =
		{
			//8 round 12 gauge shells
			"8Rnd_SPAS12_buck",
			"8Rnd_SPAS12_PPA",
			"8Rnd_SPAS12_slug"
		};
	};
	class CBA_40SW_UMP
	{
		KA_Mags[] =
		{
			//30 round UMP40 mags
			"KA_30Rnd_40SW_FMJ_Mag",
			"KA_30Rnd_40SW_JHP_Mag"
		};
	};
	class CBA_45ACP_UMP
	{
		KA_Mags[] =
		{
			//25 round UMP45 mags
			"KA_25Rnd_45ACP_FMJ_Mag",
			"KA_25Rnd_45ACP_Tracer_Green_Mag",
			"KA_25Rnd_45ACP_Tracer_Red_Mag",
			"KA_25Rnd_45ACP_High_Presure_Mag"
		};
	};
	class CBA_46x30_MP7
	{
		KA_Mags[] =
		{
			//20 & 40 round MP7 mags
			"KA_20Rnd_46x30_AP",
			"KA_20Rnd_46x30_APT",
			"KA_20Rnd_46x30_FMJ",
			"KA_20Rnd_46x30_Subsonic",
			"KA_40Rnd_46x30_AP",
			"KA_40Rnd_46x30_APT",
			"KA_40Rnd_46x30_FMJ",
			"KA_40Rnd_46x30_Subsonic"
		};
	};
	class CBA_556x45_FAMAS
	{
		KA_Mags[] =
		{
			//25 round Famas mags
			"KA_FAMAS_25rnd_SS109_FMJ_mag",
			"KA_FAMAS_25rnd_Subsonic_mag",
			"KA_FAMAS_25rnd_Tracer_Red_mag"
		};
	};
	class CBA_556x45_SCAR_EGLM
	{
		KA_Mags[] =
		{
			//30 round M4/M16/Model _randomnumber_/SCAR-L mags
			"KA_30Rnd_556x45_M862PPA",
			"KA_30Rnd_556x45_M995AP",
			"KA_30Rnd_556x45_Mk318",
			"KA_30Rnd_556x45_Mk262",
			"KA_30Rnd_556x45_M855",
			"KA_30Rnd_556x45_M855A1",
			"KA_30Rnd_556x45_M856",
			"KA_30Rnd_556x45_XM996",
			"KA_M16_30rnd_M193_Ball_mag",
			"KA_M16_30rnd_M196_Tracer_Red_mag",
			"KA_M16_30rnd_M200_Blank_mag",
			"KA_M16_30rnd_M855_FMJ_mag",
			"KA_M16_30rnd_M855A1_LFS_mag",
			"KA_M16_30rnd_M856_Tracer_Red_mag",
			"KA_M16_30rnd_M862_PPA_mag",
			"KA_M16_30rnd_M995_AP_mag",
			"KA_M16_30rnd_Mk262_SPR_mag",
			"KA_M16_30rnd_Mk318_SOST_mag",
			"KA_M16_30rnd_Subsonic_mag",
			"KA_M16_30rnd_XM1037_SRTA_mag",
			"KA_M16_30rnd_XM996_Dim_Tracer_mag",
			"KA_SCAR_L_30rnd_Mk318_SOST_mag",
			"KA_SCAR_L_30rnd_M193_Ball_mag",
			"KA_SCAR_L_30rnd_M855_FMJ_mag",
			"KA_SCAR_L_30rnd_M855A1_LFS_mag",
			"KA_SCAR_L_30rnd_Mk262_SPR_mag",
			"KA_SCAR_L_30rnd_M995_AP_mag",
			"KA_SCAR_L_30rnd_M196_Tracer_Red_mag",
			"KA_SCAR_L_30rnd_M856_Tracer_Red_mag",
			"KA_SCAR_L_30rnd_XM996_Dim_Tracer_mag",
			"KA_SCAR_L_30rnd_Subsonic_mag",
			"KA_SCAR_L_30rnd_M862_PPA_mag",
			"KA_SCAR_L_30rnd_XM1037_SRTA_mag",
			"KA_SCAR_L_30rnd_M200_Blank_mag"
		};
	};
	class CBA_556x45_SG550
	{
		KA_Mags[] =
		{
			//30 round SIG SG550/552 mags
			"KA_SIG_30rnd_M193_Ball_mag",
			"KA_SIG_30rnd_M196_Tracer_Red_mag",
			"KA_SIG_30rnd_M200_Blank_mag",
			"KA_SIG_30rnd_M855_FMJ_mag",
			"KA_SIG_30rnd_M855A1_LFS_mag",
			"KA_SIG_30rnd_M856_Tracer_Red_mag",
			"KA_SIG_30rnd_M862_PPA_mag",
			"KA_SIG_30rnd_M995_AP_mag",
			"KA_SIG_30rnd_Mk262_SPR_mag",
			"KA_SIG_30rnd_Mk318_SOST_mag",
			"KA_SIG_30rnd_Subsonic_mag",
			"KA_SIG_30rnd_XM1037_SRTA_mag",
			"KA_SIG_30rnd_XM996_Dim_Tracer_mag",
		};
	};
	class CBA_556x45_STANAG
	{
		KA_Mags[] =
		{
			//30 round M4/M16/Model _randomnumber_/SCAR-L mags
			"KA_30Rnd_556x45_M862PPA",
			"KA_30Rnd_556x45_M995AP",
			"KA_30Rnd_556x45_Mk318",
			"KA_30Rnd_556x45_Mk262",
			"KA_30Rnd_556x45_M855",
			"KA_30Rnd_556x45_M855A1",
			"KA_30Rnd_556x45_M856",
			"KA_30Rnd_556x45_XM996",
			"KA_M16_30rnd_M193_Ball_mag",
			"KA_M16_30rnd_M196_Tracer_Red_mag",
			"KA_M16_30rnd_M200_Blank_mag",
			"KA_M16_30rnd_M855_FMJ_mag",
			"KA_M16_30rnd_M855A1_LFS_mag",
			"KA_M16_30rnd_M856_Tracer_Red_mag",
			"KA_M16_30rnd_M862_PPA_mag",
			"KA_M16_30rnd_M995_AP_mag",
			"KA_M16_30rnd_Mk262_SPR_mag",
			"KA_M16_30rnd_Mk318_SOST_mag",
			"KA_M16_30rnd_Subsonic_mag",
			"KA_M16_30rnd_XM1037_SRTA_mag",
			"KA_M16_30rnd_XM996_Dim_Tracer_mag",
			"KA_SCAR_L_30rnd_Mk318_SOST_mag",
			"KA_SCAR_L_30rnd_M193_Ball_mag",
			"KA_SCAR_L_30rnd_M855_FMJ_mag",
			"KA_SCAR_L_30rnd_M855A1_LFS_mag",
			"KA_SCAR_L_30rnd_Mk262_SPR_mag",
			"KA_SCAR_L_30rnd_M995_AP_mag",
			"KA_SCAR_L_30rnd_M196_Tracer_Red_mag",
			"KA_SCAR_L_30rnd_M856_Tracer_Red_mag",
			"KA_SCAR_L_30rnd_XM996_Dim_Tracer_mag",
			"KA_SCAR_L_30rnd_Subsonic_mag",
			"KA_SCAR_L_30rnd_M862_PPA_mag",
			"KA_SCAR_L_30rnd_XM1037_SRTA_mag",
			"KA_SCAR_L_30rnd_M200_Blank_mag"
		};
	};
	class CBA_762x51_SCAR
	{
		KA_Mags[] =
		{
			"KA_SCAR_H_20rnd_Mk319_SOST_mag",
			"KA_SCAR_H_20rnd_M59_Ball_mag",
			"KA_SCAR_H_20rnd_M80_Ball_mag",
			"KA_SCAR_H_20rnd_M80A1_Ball_mag",
			"KA_SCAR_H_20rnd_M118_SPR_mag",
			"KA_SCAR_H_20rnd_M118LR_OTM_mag",
			"KA_SCAR_H_20rnd_Mk316_SPR_mag",
			"KA_SCAR_H_20rnd_M852_OTM_mag",
			"KA_SCAR_H_20rnd_M62_Tracer_Red_mag",
			"KA_SCAR_H_20rnd_M276_Dim_Tracer_mag",
			"KA_SCAR_H_20rnd_M61_AP_mag",
			"KA_SCAR_H_20rnd_M993_AP_mag",
			"KA_SCAR_H_20rnd_M948_SLAP_mag",
			"KA_SCAR_H_20rnd_Subsonic_mag",
			"KA_SCAR_H_20rnd_M973_SRTA_mag",
			"KA_SCAR_H_20rnd_M974_SRTA_T_mag",
			"KA_SCAR_H_20rnd_M82_Blank_mag"
		};
	};
	class CBA_762x51_SCAR_EGLM
	{
		KA_Mags[] =
		{
			"KA_SCAR_H_20rnd_Mk319_SOST_mag",
			"KA_SCAR_H_20rnd_M59_Ball_mag",
			"KA_SCAR_H_20rnd_M80_Ball_mag",
			"KA_SCAR_H_20rnd_M80A1_Ball_mag",
			"KA_SCAR_H_20rnd_M118_SPR_mag",
			"KA_SCAR_H_20rnd_M118LR_OTM_mag",
			"KA_SCAR_H_20rnd_Mk316_SPR_mag",
			"KA_SCAR_H_20rnd_M852_OTM_mag",
			"KA_SCAR_H_20rnd_M62_Tracer_Red_mag",
			"KA_SCAR_H_20rnd_M276_Dim_Tracer_mag",
			"KA_SCAR_H_20rnd_M61_AP_mag",
			"KA_SCAR_H_20rnd_M993_AP_mag",
			"KA_SCAR_H_20rnd_M948_SLAP_mag",
			"KA_SCAR_H_20rnd_Subsonic_mag",
			"KA_SCAR_H_20rnd_M973_SRTA_mag",
			"KA_SCAR_H_20rnd_M974_SRTA_T_mag",
			"KA_SCAR_H_20rnd_M82_Blank_mag"
		};
	};
	class CBA_9x18_PP19
	{
		KA_Mags[] =
		{
			//64 round PP-19 mags
			"KA_64Rnd_9x18_PMM_FMJ_Mag",
			"KA_64Rnd_9x18_PMM_Subsonic_Mag"
		};
	};
	class CBA_9x19_Glock_Full
	{
		KA_Mags[] =
		{
			//17 round Glock 17/18 mags
			"KA_17Rnd_9x19_Mag",
			"KA_17Rnd_9x19_Subsonic_Mag"
		};
	};
	class CBA_9x19_MP5
	{
		KA_Mags[] =
		{
			//30 round MP5 mag
			"KA_MP5_30Rnd_9x19_Subsonic_Mag"
		};
	};
	class CBA_9x19_P226
	{
		KA_Mags[] =
		{
			//15 round P226 mags
			"KA_P226_15Rnd_9x19_FMJ_Mag",
			"KA_P226_15Rnd_9x19_JHP_Mag",
			"KA_P226_15Rnd_9x19_Subsonic_Mag"
		};
	};
	class CBA_9x19_UMP
	{
		KA_Mags[] =
		{
			//30 round UMP9 mags
			"KA_30Rnd_9x19_FMJ_Mag",
			"KA_30Rnd_9x19_JHP_Mag",
			"KA_30Rnd_9x19_Subsonic_Mag"
		};
	};
	class CBA_40mm_M203
	{
		KA_Mags[] =
		{
			//40 mm grenades
			"KA_40mm_M7A3",
			"KA_40mm_MKnKJ",
			"KA_40mm_M14",
			"KA_40mm_M814"
		};
	};
	class CBA_40mm_EGLM
	{
		KA_Mags[] =
		{
			//40 mm grenades
			"KA_40mm_M7A3",
			"KA_40mm_MKnKJ",
			"KA_40mm_M14",
			"KA_40mm_M814"
		};
	};
};
