class CfgWeapons
{
	class Pistol_Base_F;
	class Rifle_Base_F;
	class Rifle_Long_Base_F: Rifle_Base_F
	{
		class WeaponSlotsInfo;
		class GunParticles;
	};
	class Rifle_Short_Base_F;
	class UGL_F;
	class XM25_base_F;
	//XM-29
	class ATA_XM29_F: XM25_base_F
	{
		class Kinetic_Energy: Rifle_Base_F
		{
			magazineWell[] = {"CBA_556x45_STANAG"};
		};
	};
	//AN94
	class AN94_base_F: Rifle_Base_F
	{
		magazineWell[] = {"CBA_545x39_AK"};
	};
	//Famas F1
	class KA_FAMAS_F1_base_F: Rifle_Base_F
	{
		magazineWell[] = {"CBA_556x45_FAMAS"};
		class KA_FAMAS_M203: UGL_F
		{
			magazineWell[] = {"CBA_40mm_M203", "CBA_40mm_EGLM"};
		};
	};
	//Glock 17
	class KA_Glock_17: Pistol_Base_F
	{
		magazineWell[] = {"CBA_9x19_Glock_Full"};
	};
	//Glock 18
	class KA_Glock_18: Pistol_Base_F
	{
		magazineWell[] = {"CBA_9x19_Glock_Full"};
	};
	//M1014
	class KA_M1014: Rifle_Base_F
	{
		magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds","CBA_12g_6rnds"};
	};
	//M16
	class KA_M16A2: Rifle_Base_F
	{
		magazineWell[] = {"CBA_556x45_STANAG"};
		class KA_M16_M203: UGL_F
		{
			magazineWell[] = {"CBA_40mm_M203", "CBA_40mm_EGLM"};
		};
	};
	//M4
	class KA_M4A1_base_F: Rifle_Base_F
	{
		magazineWell[] = {"CBA_556x45_STANAG"};
		class M203: UGL_F
		{
			magazineWell[] = {"CBA_40mm_M203", "CBA_40mm_EGLM"};
		};
	};
	//Mossberg 590
	class Mossberg_590: Rifle_Base_F
	{
		magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds","CBA_12g_6rnds","CBA_12g_7rnds","CBA_12g_8rnds"};
	};
	//MP5SD3
	class KA_MP5SD3: Rifle_Short_Base_F
	{
		magazineWell[] = {"CBA_9x19_MP5"};
	};
	//MP7
	class KA_MP7_Pistol_Black_40Rnd: Pistol_Base_F
	{
		magazineWell[] = {"CBA_46x30_MP7"};
	};
	class KA_MP7_Rifle_Black_40Rnd: Rifle_Base_F
	{
		magazineWell[] = {"CBA_46x30_MP7"};
	};
	//PP-19
	class KA_PP19_base: Rifle_Short_Base_F
	{
		magazineWell[] = {"CBA_9x18_PP19"};
	};
	//P226
	class KA_P226: Pistol_Base_F
	{
		magazineWell[] = {"CBA_9x19_P226"};
	};
	//SCAR-H
	class KA_SCAR_H_Base: Rifle_Long_Base_F
	{
		magazineWell[] = {"CBA_762x51_SCAR"};
		class MK13_EGLM: UGL_F
		{
			magazineWell[] = {"CBA_40mm_M203", "CBA_40mm_EGLM"};
		};
	};
	//SCAR-H EGLM
	class KA_SCAR_H_Black_EGLM: KA_SCAR_H_Base
	{
		magazineWell[] = {"CBA_762x51_SCAR_EGLM"};
	};
	//SCAR-L
	class KA_SCAR_L_Base: Rifle_Base_F
	{
		magazineWell[] = {"CBA_556x45_STANAG"};
		class MK13_EGLM_L: UGL_F
		{
			magazineWell[] = {"CBA_40mm_M203", "CBA_40mm_EGLM"};
		};
	};
	// SCAR-L EGLM
	class KA_SCAR_L_Black_EGLM: KA_SCAR_L_Base
	{
		magazineWell[]= {"CBA_556x45_SCAR_EGLM"};
	};
	//Sawed Off Shotgun
	class KICKASS_Sawed_Off_Shotgun: Pistol_Base_F
	{
		magazineWell[] = {"CBA_12g_1rnd","CBA_12g_2rnds"};
	};
	//SG550, SG552
	class KA_SIG_Base: Rifle_Base_F
	{
		magazineWell[] = {"CBA_556x45_SG550"};
	};
	//SPAS-12
	class KA_SPAS12: Rifle_Base_F
	{
		magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds","CBA_12g_6rnds","CBA_12g_7rnds","CBA_12g_8rnds"};
	};
	//UMP45
	class KA_UMP45_Base: Rifle_Short_Base_F
	{
		magazineWell[] = {"CBA_45ACP_UMP"};
	};
	//UMP40
	class KA_UMP40: KA_UMP45_Base
	{
		magazineWell[] = {"CBA_40SW_UMP"};
	};
	//UMP9
	class KA_UMP9: KA_UMP45_Base
	{
		magazineWell[] = {"CBA_9x19_UMP"};
	};
};
