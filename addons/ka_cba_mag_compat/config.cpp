class CfgPatches
{
	class armaforces_ka_cba_mag_compat
	{
		name = "KA CBA Compat";
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Tank_Loadorder","cba_main","KA_M4A1","KA_SCAR_H","MRC"};
		author = "3Mydlo3";
	};
};

#include "CfgWeapons.hpp"
#include "CfgMagazineWells.hpp"
